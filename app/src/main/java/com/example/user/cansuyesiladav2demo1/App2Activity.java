package com.example.kubra.denemedeneme;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class App2Activity extends Activity {

    TextToSpeech t1;
    EditText text;
    Button tts_btn;
    public int[] butArray = {R.id.n1, R.id.n2, R.id.n3, R.id.n4, R.id.n5,R.id.n6,R.id.n7,R.id.n8,R.id.n9,R.id.n10,R.id.n11,R.id.n12,R.id.n13,R.id.n14,R.id.n15,R.id.n16,R.id.n17,R.id.n18,R.id.n19,R.id.n20,R.id.n21,R.id.n22,R.id.n23,R.id.d0,R.id.d1,R.id.d2,R.id.d3,R.id.d4,R.id.d5,R.id.d6,R.id.d7,R.id.d8,R.id.d9};
    public Button[] bt= new Button[butArray.length];
    public String [] myArray = {"A", "B", "C", "D", "E", "F", "G", "H","I","J","K","L","M","N","O","P","R","S","T","U","V","Y","Z","0","1","2","3","4","5","6","7","8","9"};

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Tuşlara basıldığında click sesi
        final MediaPlayer buttonSound = MediaPlayer.create(this, R.raw.o);
        text = (EditText) findViewById(R.id.Text1);
        text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);


        //Silme tuşları
        Button clear = (Button) findViewById(R.id.clr);
        Button delete = (Button) findViewById(R.id.del);
        Button space = (Button) findViewById(R.id.space);
        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (text.getText().toString().matches("")) {
                    text.setText("");

                } else {
                    //yazılan tüm bilgileri siler
                    text.setText("");
                }


            }
        });

        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //edittext boş ise yine boş kalsı
                if (text.getText().toString().matches("")) {
                    text.setText("");
                }
                //eğer yazılmış text varsa en sondaki karakteri sil
                else {
                    text.setText(text.getText().delete(text.getText().length() - 1, text.getText().length()));
                }
                /// titresim.vibrate(80);
            }
        });

        space.setOnClickListener(new View.OnClickListener() {
            //Boşluk karakteri ekler
            @Override
            public void onClick(View v) {
                text.setText(text.getText().insert(text.getText().length(), " "));
            }
        });


        //Tuşlara basıldığında titreşim ekleme
        final Vibrator titresim = (Vibrator) App2Activity.this.getSystemService(Context.VIBRATOR_SERVICE);


        for (int i = 0; i < butArray.length; i++) {
            final int b = i;
            bt[b] = (Button) findViewById(butArray[b]);

            bt[b].setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // titresim.vibrate(80);
                    buttonSound.start();
                    Toast.makeText(getApplicationContext(), myArray[b], Toast.LENGTH_SHORT).show();

                    switch (v.getId()) {

                        case R.id.n1:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n2:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n3:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n4:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n5:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n6:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n7:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n8:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n9:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n10:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n11:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n12:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n13:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n14:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n15:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n16:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n17:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n18:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n19:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n20:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n21:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n22:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n23:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;

                        case R.id.d0:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d1:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d2:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d3:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d4:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d5:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d6:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d7:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d8:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.d9:
                            text.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;

                    }

                }
            });

        }

        // Text to speech calıstırma
        tts_btn = (Button) findViewById(R.id.button_text);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        tts_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toSpeak = text.getText().toString();
                Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

            }
        });

    }
    public void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();



    }
}
