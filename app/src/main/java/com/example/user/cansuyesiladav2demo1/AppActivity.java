package com.example.kubra.denemedeneme;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class AppActivity extends Activity {

    ImageButton imageButton;
    ImageButton imageButton2;
    ImageButton imageButton3;
    ImageButton imageButton4;
    ImageButton imageButton5;
    ImageButton imageButton6;
    Button button;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context context = this;

        imageButton =  (ImageButton) findViewById(R.id.imageButton);
        imageButton2 = (ImageButton) findViewById(R.id.imageButton2);
        imageButton3 = (ImageButton) findViewById(R.id.imageButton3);
        imageButton4 = (ImageButton) findViewById(R.id.imageButton4);
        imageButton5 = (ImageButton) findViewById(R.id.imageButton5);
        imageButton6 = (ImageButton) findViewById(R.id.imageButton6);
        button = (Button) findViewById((R.id.button));

        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton) {
                    Log.i("Send email", "");

                    String[] TO = {"kalekubra.kk@gmail.com"};
                    //String[] CC = {"rabiaery@gmail.com"};
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");


                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    //emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Informing");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "I feel hungry!");


                    try {
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        finish();
                        Log.i("Finished sending email.", "");
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(AppActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton2) {
                    Log.i("Send email", "");

                    String[] TO = {"kalekubra.kk@gmail.com"};
                    //String[] CC = {"rabiaery@gmail.com"};
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");


                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    //emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Informing");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "I feel thirsty!");


                    try {
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        finish();
                        Log.i("Finished sending email.", "");
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(AppActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        imageButton3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (v.getId() == R.id.imageButton3) {
                    Log.i("Send email", "");

                    String[] TO = {"kalekubra.kk@gmail.com"};
                    //String[] CC = {"rabiaery@gmail.com"};
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");


                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    //emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Informing");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "I feel cold!");

                    try {
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        finish();
                        Log.i("Finished sending email.", "");
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(AppActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        imageButton4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton4) {
                    Log.i("Send email", "");

                    String[] TO = {"kalekubra.kk@gmail.com"};
                    //String[] CC = {"rabiaery@gmail.com"};
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");


                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    //emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Informing");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "I sweated!");

                    try {
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        finish();
                        Log.i("Finished sending email.", "");
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(AppActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        imageButton5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton5) {
                    Log.i("Send email", "");

                    String[] TO = {"kalekubra.kk@gmail.com"};
                    //String[] CC = {"rabiaery@gmail.com"};
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");


                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    //emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Informing");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "I need to use WC!");

                    try {
                        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        finish();
                        Log.i("Finished sending email.", "");
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(AppActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });


        imageButton6.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (arg0.getId() == R.id.imageButton6) {
                    Intent intent = new Intent(context, App2Activity.class);
                    startActivity(intent);
                }
            }
        });


    }
    /*public void addListenerOnButton() {


    }*/
}




